import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root',
})
export class WatherService {
  ApiURL = 'http://api.openweathermap.org/data/2.5/weather';
  appId = '3d8b309701a13f65b660fa2c64cdc517';

  public headers: HttpHeaders;

  constructor(private http: HttpClient) {
    this.headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
  }

  getWatherDataByCity(city: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/data/2.5/weather?q=${city}&appid=${this.appId}`, {
      headers: this.headers,
    });

  }
  getWatherForcastDataByCity(city: string): Observable<any> {
    return this.http.get(`${environment.apiUrl}/data/2.5/forecast?q=${city}&appid=${this.appId}`, {
      headers: this.headers,
    });

  }
}
