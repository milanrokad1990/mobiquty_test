import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WatherService } from '../../services/wather.service';
import * as moment from "moment";

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.scss'],
})
export class ChildComponent implements OnInit {
  detailData: any = [];
  constructor(public route: ActivatedRoute, private watherService: WatherService) {
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
      if (params.city) {
        this.getWatherForcastDataByCity(params.city)
      }
    });
  }

  // Get Wather data
  async getWatherForcastDataByCity(cityName: any) {
    this.detailData = [];
    try {
      await this.watherService.getWatherForcastDataByCity(cityName).subscribe(
        (result: any) => {
          result.list.forEach((element: any) => {
            let temp = moment(element.dt_txt).format("HH");
            if (temp == '09') {
              if (this.detailData.length <= 4) {
                this.detailData.push(
                  {
                    temp: element.main.temp,
                    sea_level: element.main.sea_level
                  }
                )
              }

            }
          });
        },
        (error: any) => {
          console.error('error', error);
        }
      );
    } catch (error) {
      console.log(error);
    }
  }


}
