import { Component, OnInit } from '@angular/core';
import * as data from '../../../assets/data';
import { WatherService } from '../services/wather.service';
import * as moment from "moment";
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {

  cities: any = [];
  constructor(private watherService: WatherService, private router: Router) { }

  ngOnInit(): void {
    if (data.data.length > 0) {
      this.getWatherDataByCity(data.data)
    }
  }

  // Get Wather data
  getWatherDataByCity(citiesData: any = []) {
    try {
      this.cities = citiesData;
      this.cities.forEach((element: any) => {
        var query = element.cityname;
        this.watherService.getWatherDataByCity(query).subscribe(
          (result: any) => {
            element.temperature = result.main.temp
            element.sunriseTime = moment(result.sys.sunrise * 1000).format("DD-MM-YYYY HH:mm:ss");
            element.sunsetTime = moment(result.sys.sunset * 1000).format("DD-MM-YYYY HH:mm:ss");
          },
          (error: any) => {
            console.error('error', error);
          }
        );
      });
    } catch (error) {
      console.log(error);
    }
  }

  // Goto Detail page with city name
  gotoDetail(data: any) {
    this.router.navigate(["sub"], { queryParams: { city: data.cityname } })

  }
}
