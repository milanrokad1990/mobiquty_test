import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ErrorComponent } from './pages/error/error.component';
import { FullComponent } from "./layouts/full/full.component";
import { Full_ROUTES } from "./pages/routes/full-layout.routes";
const routes: Routes = [
  {
    path: "",
    component: FullComponent,
    children: Full_ROUTES,
  },
  {
    path: '**',
    component: ErrorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
